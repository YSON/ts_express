/**
 *
 */
import { Request, Response, Router } from 'express';
import auth from './auth';

// Instancia Enrutador Express
const routes = Router();

// test
routes.get('/test', (req: Request, res: Response) => {
  res.send('hello test');
});

routes.use('/auth', auth);

/**
 * [index description]
 * @type {[type]}
 */
// export const index = (req: Request, res: Response) => {
//     res.send('Boliparte Ts-Express');
// };

export default routes;
