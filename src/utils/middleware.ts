import { Router } from 'express';

export type Wrapper = ((router: Router) => void);

/**
 * [applyMiddleware description]
 * @type {function}
 */
export const applyMiddleware = (
    middleware: Wrapper[],
    router: Router,
) => {
    for (const f of middleware) {
        f(router);
    }
};
