/**
 * express instance for testing only
 * note: js express generator support
 * error: do not include this file in production (use npm run build)
 */

// tslint:disable-next-line
const app = require('./app');

export default app;
